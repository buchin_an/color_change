package com.example.home.colorchange;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btn_container)
    RelativeLayout btnContainer;

    List<Button> buttons;
    @BindView(R.id.first)
    Button first;
    @BindView(R.id.second)
    Button second;
    @BindView(R.id.third)
    Button third;
    @BindView(R.id.forth)
    Button forth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        buttons = new ArrayList<>();
        buttons.add(first);
        buttons.add(second);
        buttons.add(third);
        buttons.add(forth);
    }

    @OnClick({R.id.first, R.id.second, R.id.third, R.id.forth})
    public void changeColor(Button button) {
        for (int i = 0; i < buttons.size(); i++) {
            if (button != buttons.get(i)) {
                animateBetweenColors(buttons.get(i), Color.WHITE,Palette.getColor(),2000);
            }
        }
    }

    private void animateBetweenColors(final View viewToAnimateItBackground, final int colorFrom, final int colorTo,
                                            final int durationInMs) {
        final ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            ColorDrawable colorDrawable = new ColorDrawable(colorFrom);

            @Override
            public void onAnimationUpdate(final ValueAnimator animator) {
                colorDrawable.setColor((Integer) animator.getAnimatedValue());
                viewToAnimateItBackground.setBackground(colorDrawable);
            }
        });
        if (durationInMs >= 0)
            colorAnimation.setDuration(durationInMs);
        colorAnimation.start();
    }
    @OnClick({R.id.first, R.id.second, R.id.third, R.id.forth})

    public void rotate(){
        Animation turn= AnimationUtils.loadAnimation(this,R.anim.rotate_animation);
        btnContainer.startAnimation(turn);
    }
}
