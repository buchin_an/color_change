package com.example.home.colorchange;

import android.graphics.Color;

import java.util.ArrayList;

public final class Palette {
    private static ArrayList<String> colors = new ArrayList<>();
    private static int count = 0;

     static  {
        colors.add("#004D40");
        colors.add("#00695C");
        colors.add("#00796B");
        colors.add("#00897B");
        colors.add("#009688");
        colors.add("#26A69A");
        colors.add("#4DB6AC");
        colors.add("#80CBC4");
        colors.add("#B2DFDB");
        colors.add("#E0F2F1");
        colors.add("#80DEEA");
        colors.add("#26C6DA");
        colors.add("#00BCD4");
        colors.add("#00838F");
    }

    public static int getColor() {

        if (count < colors.size()-1) count++;
        else count = 0;
        return Color.parseColor(colors.get(count));
    }



}
